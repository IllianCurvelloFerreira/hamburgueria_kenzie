import React from "react";

class Product extends React.Component {
  render() {
    const { name, category, price } = this.props.product;
    return (
      <div>
        <div>
          <h3>{name}</h3>
          <p>{`Caegoria: ${category}`}</p>
          <p>{`Preço: ${price} R$`}</p>
        </div>
      </div>
    );
  }
}

export default Product;
