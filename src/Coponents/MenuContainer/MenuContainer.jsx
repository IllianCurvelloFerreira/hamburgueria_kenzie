import React from "react";
import Product from "./Product";

class MenuContainer extends React.Component {
  render() {
    return (
      <div>
        {this.props.showSelected &&
          this.props.products.map((product) => (
            <>
              <Product product={product} />
              {this.props.haveButton && (
                <button onClick={this.props.click} id={product.id}>
                  Adicionar
                </button>
              )}
            </>
          ))}
      </div>
    );
  }
}

export default MenuContainer;
