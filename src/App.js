import "./App.css";
import { Component } from "react";
import MenuContainer from "./Coponents/MenuContainer/MenuContainer";

class App extends Component {
  state = {
    products: [
      { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
      { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
      { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
      { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
      { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
      { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
      { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
    ],
    value: "",
    filteredProducts: [],
    currentSale: { total: 0, saleDetails: [] },
    showSale: true,
  };

  procura = { search: "" };

  myChangeHandler = (event) => {
    this.setState({ search: event.target.value });
  };

  showProducts = () => {
    if (this.state.value === "") {
      this.setState({ filteredProducts: this.state.products });
    } else {
      this.setState({
        filteredProducts: this.state.products.filter((e) => {
          let str1 = e.name
            .toLocaleLowerCase("en-US")
            .replace(/[^a-z0-9]/gi, "")
            .trim();
          let str2 = this.state.value
            .toLocaleLowerCase("en-US")
            .replace(/[^a-z0-9]/gi, "")
            .trim();
          console.log(`${str1} e ${str2}`);
          console.log(str1 === str2);
          return str1 === str2;
        }),
      });
      return;
    }
  };

  handleClick = (event) => {
    const item = this.state.products.find(
      (e) => e.id === parseInt(event.target.id)
    );
    this.setState({
      currentSale: {
        total: 0,
        saleDetails: [...this.state.currentSale.saleDetails, item],
      },
    });
  };

  showSale = () => {
    this.state.showSale
      ? this.setState({ showSale: false })
      : this.setState({ showSale: true });
  };

  render() {
    const { filteredProducts, products, showSale } = this.state;
    const subTotal = (
      <p>
        {this.state.currentSale.saleDetails.reduce(
          (acc, e) => (acc += e.price),
          0
        )}
      </p>
    );
    return (
      <>
        <input
          value={this.state.value}
          onChange={(event) => this.setState({ value: event.target.value })}
        ></input>
        <button onClick={this.showProducts}>Pesquisar</button>
        <MenuContainer
          products={filteredProducts.length > 0 ? filteredProducts : products}
          click={this.handleClick}
          haveButton
          showSelected
        />
        <p>Subtotal: {subTotal} </p>
        <button onClick={this.showSale}>
          {this.state.showSale ? "Hide sale" : "Show sale"}
        </button>
        {showSale && (
          <MenuContainer
            products={this.state.currentSale.saleDetails}
            click={this.handleClick}
            showSelected
          />
        )}
      </>
    );
  }
}

export default App;
